import 'dart:async';

import 'package:greencat/greencat.dart';
import 'package:greencat_epics/greencat_epics.dart';
import 'package:test/test.dart';

import 'test_utils.dart';

main() {
  group('Epic Middleware', () {
    Store<List<Action>, Action> store;

    tearDown(() {
      store.close();
    });

    test('accepts an Epic that transforms one Action into another', () {
      final epicMiddleware = new EpicMiddleware(new Fire1Epic());
      store = new Store.createStore(reducer, initialState: <Action>[])
        ..addMiddleware(epicMiddleware);

      store.dispatch(new EpicAction.send1());

      expect(store.state,
          equals([new EpicAction.send1(), new EpicAction.respond1()]));
    });

    test('can combine Epics', () {
      final epic = new CombinedEpic([new Fire1Epic(), new Fire2Epic()]);
      final epicMiddleware = new EpicMiddleware(epic);
      store = new Store.createStore(reducer, initialState: <Action>[])
        ..addMiddleware(epicMiddleware);

      store.dispatch(new EpicAction.send1());
      store.dispatch(new EpicAction.send2());

      expect(
          store.state,
          equals([
            new EpicAction.send1(),
            new EpicAction.respond1(),
            new EpicAction.send2(),
            new EpicAction.respond2()
          ]));
    });

    test('work with async epics', () async {
      final epicMiddleware = new EpicMiddleware(new CancelableEpic());
      store = new Store.createStore(reducer, initialState: <Action>[])
        ..addMiddleware(epicMiddleware);

      store.dispatch(new EpicAction.send1());

      await new Future.delayed(new Duration(milliseconds: 10));

      expect(store.state,
          equals([new EpicAction.send1(), new EpicAction.respond1()]));
    });

    test('work with cancelable async epics', () async {
      final epicMiddleware = new EpicMiddleware(new CancelableEpic());
      store = new Store.createStore(reducer, initialState: <Action>[])
        ..addMiddleware(epicMiddleware);

      store.dispatch(new EpicAction.send1());
      store.dispatch(new EpicAction.send2());

      await new Future.delayed(new Duration(milliseconds: 10));

      expect(store.state,
          equals([new EpicAction.send1(), new EpicAction.send2()]));

      store.dispatch(new EpicAction.send1());
      // No `send2` dispatched, which cancels the `respond1` action from firing

      await new Future.delayed(new Duration(milliseconds: 10));

      expect(
          store.state,
          equals([
            new EpicAction.send1(),
            new EpicAction.send2(),
            new EpicAction.send1(),
            new EpicAction.respond1()
          ]));

      store.dispatch(new EpicAction.send1());
      store.dispatch(new EpicAction.send2());

      await new Future.delayed(new Duration(milliseconds: 10));

      expect(
        store.state,
        equals(
          [
            new EpicAction.send1(),
            new EpicAction.send2(),
            new EpicAction.send1(),
            new EpicAction.respond1(),
            new EpicAction.send1(),
            new EpicAction.send2()
          ],
        ),
      );
    });

    test('can replace the current Epic', () {
      final originalEpic = new Fire1Epic();
      final replacementEpic = new Fire2Epic();
      final epicMiddleware = new EpicMiddleware(originalEpic);
      store = new Store.createStore(reducer, initialState: <Action>[])
        ..addMiddleware(epicMiddleware);

      expect(epicMiddleware.epic, equals(originalEpic));

      epicMiddleware.epic = replacementEpic;

      store.dispatch(new EpicAction.send1());
      store.dispatch(new EpicAction.send2());

      expect(epicMiddleware.epic, equals(replacementEpic));
      expect(
          store.state,
          equals([
            new EpicAction.send1(),
            new EpicAction.send2(),
            new EpicAction.respond2()
          ]));
    });

    test('can fire multiple events from epics', () async {
      final epicMiddleware = new EpicMiddleware(new FireTwoActionsEpic());
      store = new Store.createStore(reducer, initialState: <Action>[])
        ..addMiddleware(epicMiddleware);

      store.dispatch(new EpicAction.send1());

      await new Future.delayed(new Duration(milliseconds: 1));

      expect(store.state,
          equals([new EpicAction.send1(), new EpicAction.respond1()]));

      await new Future.delayed(new Duration(milliseconds: 10));

      expect(
          store.state,
          equals([
            new EpicAction.send1(),
            new EpicAction.respond1(),
            new EpicAction.respond2()
          ]));
    });

    test('passes the current state of the store to the Epic', () {
      final epic = new RecordingEpic();
      final epicMiddleware = new EpicMiddleware(epic);
      final initialState = [new EpicAction.respond1()];
      store = new Store.createStore(reducer, initialState: initialState)
        ..addMiddleware(epicMiddleware);

      store.dispatch(new EpicAction.send1());

      expect(epic.store.state, equals(initialState));
    });
  });
}
