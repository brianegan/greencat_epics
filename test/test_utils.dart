import 'dart:async';

import 'package:greencat/greencat.dart';
import 'package:greencat_epics/greencat_epics.dart';
import 'package:rxdart/rxdart.dart';

enum ActionType {
  SEND1,
  SEND2,
  RESPOND1,
  RESPOND2,
}

class EpicAction extends Action {
  final ActionType type;

  EpicAction(this.type);

  factory EpicAction.send1() => new EpicAction(ActionType.SEND1);
  factory EpicAction.send2() => new EpicAction(ActionType.SEND2);
  factory EpicAction.respond1() => new EpicAction(ActionType.RESPOND1);
  factory EpicAction.respond2() => new EpicAction(ActionType.RESPOND2);

  @override
  get payload => type;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is EpicAction &&
          runtimeType == other.runtimeType &&
          type == other.type;

  @override
  int get hashCode => type.hashCode;
}

List<Action> reducer(Action action, {List<Action> currentState}) {
  currentState.add(action);

  return currentState;
}

class Fire1Epic extends Epic<List<Action>, Action> {
  @override
  Stream<Action> map(
      Stream<Action> actions, EpicStore<List<Action>, Action> store) {
    return actions
        .where((action) => action.payload == ActionType.SEND1)
        .map((action) => new EpicAction.respond1());
  }
}

class Fire2Epic extends Epic<List<Action>, Action> {
  @override
  Stream<Action> map(
      Stream<Action> actions, EpicStore<List<Action>, Action> store) {
    return actions
        .where((action) => action.payload == ActionType.SEND2)
        .map((action) => new EpicAction.respond2());
  }
}

class CancelableEpic extends Epic<List<Action>, Action> {
  @override
  Stream<Action> map(
          Stream<Action> actions, EpicStore<List<Action>, Action> store) =>
      new Observable(actions)
          .where((action) => action.payload == ActionType.SEND1)
          .flatMap((action) => new Observable.just(new EpicAction.respond1())
              .debounce(new Duration(milliseconds: 1))
              .takeUntil(actions
                  .where((action) => action.payload == ActionType.SEND2)));
}

class FireTwoActionsEpic extends Epic<List<Action>, Action> {
  @override
  Stream<Action> map(
      Stream<Action> actions, EpicStore<List<Action>, Action> store) {
    return new Observable(actions)
        .where((action) => action.payload == ActionType.SEND1)
        .flatMap((action) => new Observable.merge([
              new Observable.just(new EpicAction.respond1()),
              new Observable.just(new EpicAction.respond2())
                  .debounce(new Duration(milliseconds: 5))
            ]));
  }
}

class RecordingEpic extends Epic<List<Action>, Action> {
  EpicStore<List<Action>, Action> store;

  @override
  Stream<Action> map(
      Stream<Action> actions, EpicStore<List<Action>, Action> store) {
    this.store = store;

    return actions;
  }
}
