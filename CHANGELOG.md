# Changelog

## 0.0.1

- Initial version, includes `Epic`, `EpicMiddleware`, and `CombinedEpic`.
- Basic documentation
- Tests included!
