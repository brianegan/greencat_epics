import 'dart:async';

import 'package:greencat/greencat.dart';
import 'package:greencat_epics/src/epic.dart';
import 'package:greencat_epics/src/epic_store.dart';
import 'package:rxdart/transformers.dart';

/// A [Greencat](https://pub.dartlang.org/packages/greencat) middleware that
/// passes a stream of dispatched actions to the given [Epic].
///
/// It is recommended that you put your `EpicMiddleware` first when constructing
/// the list of middleware for your store so any actions dispatched from
/// your [Epic] will be intercepted by the remaining Middleware.
///
/// Example:
///
///     final epicMiddleware = new EpicMiddleware(new ExampleEpic());
///     final store = new Store.createStore(reducer)
///       ..addMiddleware(epicMiddleware);
class EpicMiddleware<S, A extends Action> implements Function {
  final StreamController<A> _actions =
      new StreamController.broadcast(sync: true);
  final StreamController<Epic<S, A>> _epics =
      new StreamController.broadcast(sync: true);

  Epic<S, A> _epic;
  bool _isSubscribed = false;

  EpicMiddleware(this._epic);

  DispatchTransformer<A> call(MiddlewareApi<S, A> api) => (next) => (action) {
        if (!_isSubscribed) {
          _epics.stream
              .transform(new FlatMapLatestStreamTransformer(
                  (epic) => epic.map(_actions.stream, new EpicStore(api))))
              .listen((action) => next(action));

          _epics.add(_epic);

          _isSubscribed = true;
        }

        next(action);
        _actions.add(action);
      };

  /// Gets or replaces the epic currently used by the middleware.
  ///
  /// Replacing epics is considered an advanced API. You might need this if your
  /// app grows large and want to instantiate Epics on the fly, rather than
  /// as a whole up front.
  Epic<S, A> get epic => _epic;

  set epic(Epic<S, A> newEpic) {
    _epic = newEpic;

    _epics.add(newEpic);
  }
}
