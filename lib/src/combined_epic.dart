import 'dart:async';

import 'package:greencat/greencat.dart';
import 'package:greencat_epics/src/epic.dart';
import 'package:greencat_epics/src/epic_middleware.dart';
import 'package:greencat_epics/src/epic_store.dart';
import 'package:rxdart/streams.dart';

/// Combines a list of [Epic]s into one.
///
/// Rather than having one massive [Epic] that handles every possible type of
/// action, it's best to break [Epic]s down into smaller, more manageable and
/// testable units. This way we could have a `SearchEpic`, a `ChatEpic`,
/// and an `UpdateProfileEpic`, for example.
///
/// However, the [EpicMiddleware] accepts only one [Epic]. So what are we to do?
/// Fear not: greencat_epics includes class for combining [Epic]s together!
///
/// Example:
///
///     var epic = new CombinedEpic<State, Action>([
///       new SearchEpic(),
///       new ChatEpic(),
///       new UpdateProfileEpic()]);
class CombinedEpic<S, A extends Action> extends Epic<S, A> {
  final List<Epic<S, A>> _epics;

  CombinedEpic(List<Epic<S, A>> epics) : _epics = epics {
    assert(this._epics != null);
  }

  @override
  Stream<A> map(Stream<A> actions, EpicStore<S, A> store) {
    return new MergeStream(_epics.map((epic) => epic.map(actions, store)));
  }
}
