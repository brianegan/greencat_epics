import 'package:greencat/greencat.dart';
import 'package:greencat_epics/src/epic.dart';

/// A stripped-down Greencat [Store]. Removes unsupported [Store] methods.
///
/// Due to the way streams are implemented with Dart, it's impossible to
/// perform `store.dispatch` from within an [Epic] or observe the store directly.
class EpicStore<S, A extends Action> {
  final MiddlewareApi<S, A> api;

  EpicStore(this.api);

  /// Returns the current state of the store
  S get state => api.state;
}
