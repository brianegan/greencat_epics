# Contributing

There are two great ways to contribute: By filing issues or sending Merge Requests!

## Filing Issues

If you encounter a bug or have a question about the library, please feel free to file an issue!

## Merge Requests

If you'd like to fix a bug or improve the code in this library, we happily accept Merge Requests :) For large refactors or api-breaking changes, please consider filing an issue before writing code so the changes can be discussed!

If you're interested in submitting a Merge Request, please follow these guidelines:

  - Ensure there are no analyzer errors
  - Format your code with dartfmt
  - Update tests to cover any new code
