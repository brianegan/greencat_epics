# Greencat Epics [![build status](https://gitlab.com/brianegan/greencat_epics/badges/master/build.svg)](https://gitlab.com/brianegan/greencat_epics/commits/master)  [![coverage report](https://gitlab.com/brianegan/greencat_epics/badges/master/coverage.svg)](https://brianegan.gitlab.io/greencat_epics/)

[Greencat](https://pub.dartlang.org/packages/greencat) is a Redux implementation for Dart, and specializes in synchronous updates to a store in response to actions. However, it has only limited built-in mechanisms for asynchronous operations (thunks). When one needs to handle more complex async code, such as making an api call or retrieving information from a database in response to an action, you may require a slightly more powerful toolkit. This is where Epics come in!

The best part: Epics are based on Dart Streams. This makes routine tasks easy, and complex tasks such as asynchronous error handling, cancellation, and debouncing a breeze.

## Example

Let's say your app has a search box. When a user submits a search term,
you dispatch a `PerformSearchAction` which contains the term. In order to
actually listen for the `PerformSearchAction` and make a network request
for the results, we can create an Epic!

In this instance, our Epic will need to filter all incoming actions it
receives to only the `Action` it is interested in: the `PerformSearchAction`.
Then, we need to make a network request using the provided search term (the payload).

Finally, we need to transform those results into a new action that contains
the search results. If an error has occurred, we'll want to return a new error 
action so our app can respond accordingly.

Here's what this looks like in code.

```dart
import 'dart:async';
import 'package:greencat_epics/greencat_epics.dart';

class ExampleEpic extends Epic<State, Action> {
   @override
   Stream<Action> map(Stream<Action> actions, EpicStore<State, Action> store) {
    // Of all the actions that are dispatched
    return actions
      // Filter down to only PerformSearchActions 
      .where((action) => action is PerformSearchAction)
      // Then make an api call with the payload
      .asyncMap((action) => 
        // Pseudo api that returns a Future of SearchResults
        api.search(action.payload)
          .then((results) => new SearchResultsAction(results))
          .catchError((error) => new SearchErrorAction(error)));
  }
}
```

### Connecting the Epic to the Greencat Store

Now that we've got an epic to work with, we need to wire it up to our Greencat store so it can receive a stream of actions. In order to do this, we'll employ the `EpicMiddleware`.

```dart
import 'package:greencat_epics/greencat_epics.dart';
import 'package:greencat/greencat.dart';

final epicMiddleware = new EpicMiddleware(new ExampleEpic());
final store = new Store.createStore(exampleReducer)
  ..addMiddleware(epicMiddleware);
```

## Combining Epics

Rather than having one massive Epic that handles every possible type of action, it's best to break Epics down into smaller, more manageable and testable units. This way we could have a `SearchEpic`, a `ChatEpic`, and an `UpdateProfileEpic`, for example. 

However, the `EpicMiddleware` accepts only one Epic. So what are we to do? Fear not: greencat_epics includes class for combining Epics together!

```dart
import 'package:greencat_epics/greencat_epics.dart';

final epic = new CombinedEpic<State, Action>([
  new SearchEpic(), 
  new ChatEpic(), 
  new UpdateProfileEpic()]);
```

## Recipes

### Cancellation

In certain cases, you may need to cancel an asynchronous task in response to a dispatched action. For example, your app begins loading data in response to a user clicking on a the search button by dispatching a `PerformSearchAction`, and then the user hit's the cancel button in order to correct the search term, and your app dispatches a `CancelSearchAction`. We want our epic to cancel the previous search in response to the action. So how can we accomplish this?

This is where streams really shine. In the following example, we'll employ the RxDart library to beef up the power of streams a bit, using the `takeUntil` operator.

```dart
import 'package:greencat_epics/greencat_epics.dart';
import 'package:rxdart/rxdart.dart';

// This class is almost identical to ExampleEpic above
class CancelableSearchEpic extends Epic<State, Action> {
  @override
  Stream<Action> map(Stream<Action> actions, EpicStore<State, Action> store) =>
      new Observable(actions)
        .where((action) => action is PerformSearchAction)
        .asyncMap((action) => 
          api.search(action.payload)
            .then((results) => new SearchResultsAction(results))
            .catchError((error) => new SearchErrorAction(error)))
            
        // This is the trick. We use the takeUntil operator 
        // from RxDart to cancel the async operation in response 
        // to a CancelSearchAction
        .takeUntil(actions.where((action) => action is CancelSearchAction));
}
```
